#include <k/kstd.h>
#include "idt.h"
#include "io.h"

t_idt idt[IDT_SIZE];
unsigned long idt_info[2];

void(*interrupt_handlers[IDT_SIZE])();
void(*trap_handlers[IDT_SIZE])();

void add_interrupt_gate(u8 vector, unsigned long addr, u8 dpl)
{
  if (dpl > 3 || vector >= IDT_SIZE)
    return;
  
  idt[vector].offset_lowerbits = addr & 0xffff;
  idt[vector].selector = 0x08;
  idt[vector].zero = 0;
  idt[vector].type_attr = 0x8e | (dpl << 5);
  idt[vector].offset_higherbits = (addr & 0xffff0000) >> 16;
}

void add_trap_gate(u8 vector, unsigned long addr, u8 dpl)
{
  if (dpl > 3 || vector >= IDT_SIZE)
    return;

  idt[vector].offset_lowerbits = addr & 0xffff;
  idt[vector].selector = 0x08;
  idt[vector].zero = 0;
  idt[vector].type_attr = 0x8f | (dpl << 5);
  idt[vector].offset_higherbits = (addr & 0xffff0000) >> 16;
}

void clean_idt(unsigned long def_int)
{
  /*  for (size_t i = 0; i < IDT_SIZE; i++)
    {
      add_interrupt_gate(i, def_int, 0);
      }*/
}

void interrupt_handler(t_handler_params params)
{
  /* Called from asm */
  /* Context already saved */
  /* Calls relevant handler */

  //  kprint("Caught interrupt\n\r");

  if (params.vector <= 20)
    trap_handlers[params.vector]();
  else
    interrupt_handlers[params.vector]();
}

void remap_pic(void)
{
  /* Remapping the PIC */

  /* Saving masks */
  
  u8 pic_master = inb(PIC_MASTER_B);
  u8 pic_slave = inb(PIC_SLAVE_B);
  
  outb(PIC_MASTER_A, PIC_ICW1 | PIC_ICW1_ICW4);
  outb(PIC_SLAVE_A, PIC_ICW1 | PIC_ICW1_ICW4);
  
  outb(PIC_MASTER_B, PIC_ICW2_MASTER);
  outb(PIC_SLAVE_B, PIC_ICW2_SLAVE);
  
  outb(PIC_MASTER_B, PIC_ICW3_MASTER);
  outb(PIC_SLAVE_B, PIC_ICW3_SLAVE);
  
  outb(PIC_MASTER_B, PIC_ICW4);
  outb(PIC_SLAVE_B, PIC_ICW4);

  /* Restoring masks */
  
  outb(PIC_MASTER_B, pic_master);
  outb(PIC_SLAVE_B, pic_slave);
}

void map_interrupts(void)
{
  // Assembly interrupt wrapper

  extern int interrupt_wrapper();
  
  // Trap gates

  extern int interrupt0();
  extern int interrupt1();
  extern int interrupt2();
  extern int interrupt3();
  extern int interrupt4();
  extern int interrupt5();
  extern int interrupt6();
  extern int interrupt7();
  extern int interrupt8();
  extern int interrupt9();
  extern int interrupt10();
  extern int interrupt11();
  extern int interrupt12();
  extern int interrupt13();
  extern int interrupt14();
  extern int interrupt16();
  extern int interrupt17();
  extern int interrupt18();
  extern int interrupt19();
  extern int interrupt20();

  // Hardware interrupt gates

  extern int interrupt32();
  extern int interrupt33();
  extern int interrupt34();
  extern int interrupt35();
  extern int interrupt36();
  extern int interrupt37();
  extern int interrupt38();
  extern int interrupt39();
  extern int interrupt40();
  extern int interrupt41();
  extern int interrupt42();
  extern int interrupt43();
  extern int interrupt44();
  extern int interrupt45();
  extern int interrupt46();
  extern int interrupt47();

  // Syscall gate

  extern int interrupt128();

  // Exceptions
  
  add_trap_gate(0, (unsigned long)interrupt0, 0);
  add_trap_gate(1, (unsigned long)interrupt1, 0);
  add_trap_gate(2, (unsigned long)interrupt2, 0);
  add_trap_gate(3, (unsigned long)interrupt3, 0);
  add_trap_gate(4, (unsigned long)interrupt4, 0);
  add_trap_gate(5, (unsigned long)interrupt5, 0);
  add_trap_gate(6, (unsigned long)interrupt6, 0);
  add_trap_gate(7, (unsigned long)interrupt7, 0);
  add_trap_gate(8, (unsigned long)interrupt8, 0);
  add_trap_gate(9, (unsigned long)interrupt9, 0);
  add_trap_gate(10, (unsigned long)interrupt10, 0);
  add_trap_gate(11, (unsigned long)interrupt11, 0);
  add_trap_gate(12, (unsigned long)interrupt12, 0);
  add_trap_gate(13, (unsigned long)interrupt13, 0);
  add_trap_gate(14, (unsigned long)interrupt14, 0);
  add_trap_gate(16, (unsigned long)interrupt16, 0);
  add_trap_gate(17, (unsigned long)interrupt17, 0);
  add_trap_gate(18, (unsigned long)interrupt18, 0);
  add_trap_gate(19, (unsigned long)interrupt19, 0);
  add_trap_gate(20, (unsigned long)interrupt20, 0);

  // IRQs

  add_interrupt_gate(32, (unsigned long)interrupt32, 0);
  add_interrupt_gate(33, (unsigned long)interrupt33, 0);
  add_interrupt_gate(34, (unsigned long)interrupt34, 0);
  add_interrupt_gate(35, (unsigned long)interrupt35, 0);
  add_interrupt_gate(36, (unsigned long)interrupt36, 0);
  add_interrupt_gate(37, (unsigned long)interrupt37, 0);
  add_interrupt_gate(38, (unsigned long)interrupt38, 0);
  add_interrupt_gate(39, (unsigned long)interrupt39, 0);
  add_interrupt_gate(40, (unsigned long)interrupt40, 0);
  add_interrupt_gate(41, (unsigned long)interrupt41, 0);
  add_interrupt_gate(42, (unsigned long)interrupt42, 0);
  add_interrupt_gate(43, (unsigned long)interrupt43, 0);
  add_interrupt_gate(44, (unsigned long)interrupt44, 0);
  add_interrupt_gate(45, (unsigned long)interrupt45, 0);
  add_interrupt_gate(46, (unsigned long)interrupt46, 0);
  add_interrupt_gate(47, (unsigned long)interrupt47, 0);

  // Syscall (0x80)

  add_interrupt_gate(128, (unsigned long)interrupt128, 0);
}

void init_idt(void)
{
  extern int load_idt();
  
  clean_idt((unsigned long)default_handler);

  fill_trap_gates();
  fill_interrupt_gates();

  remap_pic();
  map_interrupts();

  // IDT descriptor
  idt_info[0] = (sizeof (t_idt) * IDT_SIZE) + (((unsigned long)idt & 0xffff) << 16);
  idt_info[1] = (unsigned long)idt >> 16 ;

  load_idt(idt_info);

  kprint("IDT initialized!\n\r");
}
