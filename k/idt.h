#ifndef IDT_H_
#define IDT_H_

#include <k/types.h>

#define IDT_SIZE	256

/* PIC-related values */

#define PIC_MASTER_A		0x20
#define PIC_SLAVE_A		0xA0
#define PIC_MASTER_B		(PIC_MASTER_A + 1)
#define PIC_SLAVE_B		(PIC_SLAVE_A + 1)

#define PIC_ICW1		0x10
#define PIC_ICW1_ICW4		0x01
#define PIC_ICW1_SINGLE		0x02
#define PIC_ICW1_LEVEL		0x08

#define PIC_ICW2_MASTER		0x20
#define PIC_ICW2_SLAVE		0x28

#define PIC_ICW3_MASTER		0x04
#define PIC_ICW3_SLAVE		0x02

#define PIC_ICW4		0x01
#define PIC_ICW4_AUTO		0x02
#define PIC_ICW4_MASTER_BUF	0x08
#define PIC_ICW4_SLAVE_BUF	0x0C
#define PIC_ICW4_NEST		0x10


void init_idt();

extern void(*interrupt_handlers[IDT_SIZE])();
extern void(*trap_handlers[IDT_SIZE])();

extern void default_handler();

extern void fill_trap_gates();
extern void fill_interrupt_gates();

typedef struct __attribute__((__packed__)) s_idt
{
  u16 offset_lowerbits;
  u16 selector;
  u8 zero;
  u8 type_attr;
  u16 offset_higherbits;
} t_idt;

typedef struct __attribute__((__packed__)) s_handler_params
{
  u32 ds;
  
  u32 edi;
  u32 esi;
  u32 ebp;
  u32 esp;
  u32 ebx;
  u32 edx;
  u32 ecx;
  u32 eax;
  
  u32 vector;
  u32 error_code;

  u32 eip;
  u32 cs;
  u32 eflags;
  u32 useresp;
  u32 ss;
} t_handler_params;

extern t_idt idt[IDT_SIZE];
extern unsigned long idt_info[2];

#endif /* !IDT_H_ */
