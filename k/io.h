#ifndef IO_H_
#define IO_H_

#include <k/types.h>

#define PORT_COM1 0x3f8
#define PORT_COM2 0x2F8
#define PORT_COM3 0x3E8
#define PORT_COM4 0x2E8

#define UART_IER_DLH	1
#define UART_IIR	2
#define UART_LCR	3
#define UART_MCR	4
#define UART_LSR	5
#define UART_MSR	6
#define UART_SR		7

static inline void outb(u16 port, u8 val)
{
  asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

static inline u8 inb(u16 port)
{
  u8 ret;
  asm volatile ( "inb %1, %0"
		 : "=a"(ret)
		 : "Nd"(port) );
  return ret;
}

void init_serial();
int kwrite(const char *buf, size_t count);
int kprint(const char *buf);

#endif /* !IO_H_ */
