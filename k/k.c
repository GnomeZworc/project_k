/*
 * Copyright (c) LSE
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY LSE AS IS AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LSE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <k/kstd.h>
#include "multiboot.h"

#include "io.h"
#include "gdt.h"
#include "idt.h"
#include "keyboard.h"

void init_gdt()
{
  add_gdt_entry(0, 0, 0, 0, 0); // Null descriptor
  add_gdt_entry(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Kernel code
  add_gdt_entry(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Kernel data
  add_gdt_entry(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User code
  add_gdt_entry(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User data
  
  gdt_info.limit = (sizeof(t_gdt) * GDT_SIZE) - 1;
  gdt_info.base = &gdt;

  load_gdt();

  kwrite("GDT Loaded!\n\r", 13);
}

void k_main(unsigned long magic, multiboot_info_t *info)
{
  (void)magic;
  (void)info;

  char star[4] = "|/-\\";
  char *fb = (void *)0xb8000;

  init_keymap();
  init_serial();
  init_gdt();
  init_idt();

  u8 keycode;
  char c;
  
  for (unsigned i = 0; ; ) {
    *fb = star[i++ % 4];

    keycode = pop_key();
    c = keymap[keycode];

    if (keycode != BUFFER_DEFAULT)
      kwrite(&c, 1);
  }
  
  for (;;)
    {
      asm volatile ("hlt");
    }
}
