#ifndef GDT_H_
#define GDT_H_

#define GDT_SIZE 5

typedef struct __attribute__((__packed__)) s_gdt_info
{
  u16 limit;
  size_t base;
} t_gdt_info;

typedef struct __attribute__((__packed__)) s_gdt
{
  u16 limit1;
  u16 base1;
  u8 base2;
  u8 access;
  u8 granularity;
  u8 base3;
} t_gdt;

t_gdt gdt[GDT_SIZE];
t_gdt_info gdt_info;

void add_gdt_entry(size_t index, unsigned long base, unsigned long limit, u8 access, u8 gran)
{
  gdt[index].base1 = (base & 0xFFFF);
  gdt[index].base2 = (base >> 16) & 0xFF;
  gdt[index].base3 = (base >> 24) & 0xFF;

  gdt[index].limit1 = (limit & 0xFFFF);
  gdt[index].granularity = ((limit >> 16) & 0x0F); // limit high and flags

  gdt[index].granularity |= (gran & 0xF0);
  gdt[index].access = access;
}

extern void load_gdt();

#endif /* !GDT_H_ */
