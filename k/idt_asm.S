	.intel_syntax noprefix

	.global load_idt
	.extern interrupt_handler
	.extern irq_handler_32


	// Macros to create basic interrupt callbacks
	// First, it deactivates interrupts
	// If there's no error code, a padding byte is pushed on the stack
	// It then pushes the interrupt vector and jumps to a generic wrapper
	
	.macro INTERRUPT_NOERROR vector
		.global interrupt\vector
	interrupt\vector\():
			cli
			push 0
			push \vector
			jmp interrupt_wrapper
	.endm

	.macro INTERRUPT_ERROR vector
		.global interrupt\vector
		interrupt\vector\():
			cli
			push \vector
			jmp interrupt_wrapper
	.endm

	INTERRUPT_NOERROR 0
	INTERRUPT_NOERROR 1
	INTERRUPT_NOERROR 2
	INTERRUPT_NOERROR 3
	INTERRUPT_NOERROR 4
	INTERRUPT_NOERROR 5
	INTERRUPT_NOERROR 6
	INTERRUPT_NOERROR 7
	INTERRUPT_ERROR 8
	INTERRUPT_NOERROR 9
	INTERRUPT_ERROR 10
	INTERRUPT_ERROR 11
	INTERRUPT_ERROR 12
	INTERRUPT_ERROR 13
	INTERRUPT_ERROR 14
	INTERRUPT_NOERROR 16
	INTERRUPT_NOERROR 17
	INTERRUPT_NOERROR 18
	INTERRUPT_NOERROR 19
	INTERRUPT_NOERROR 20

	INTERRUPT_NOERROR 32
	INTERRUPT_NOERROR 33
	INTERRUPT_NOERROR 34
	INTERRUPT_NOERROR 35
	INTERRUPT_NOERROR 36
	INTERRUPT_NOERROR 37
	INTERRUPT_NOERROR 38
	INTERRUPT_NOERROR 39
	INTERRUPT_NOERROR 40
	INTERRUPT_NOERROR 41
	INTERRUPT_NOERROR 42
	INTERRUPT_NOERROR 43
	INTERRUPT_NOERROR 44
	INTERRUPT_NOERROR 45
	INTERRUPT_NOERROR 46
	INTERRUPT_NOERROR 47

	INTERRUPT_NOERROR 128


	// Common asm wrapper
	
interrupt_wrapper:
	pusha				// Saves context

	mov ax, ds			// Saves current segment
	push eax

	mov ax, 0x10			// Changes segment to kernel mode
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	
	cld				// Clears direction flag (per convention)
	call interrupt_handler		// Calls C handler

	pop ebx				// Restores segment
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx
	
	popa				// Restores context
	add esp, 8			// Cleans pushed error code/vector
	sti				// Reactivates interrupts
	iret

load_idt:
	mov edx, [esp + 4]
	lidt [edx]
	sti
	ret
