#include "keyboard.h"
char keymap[256];

u16 push_index;
u16 pop_index;
u8 key_buffer[256];

void init_keymap()
{
  char *map_azerty = "aa1234567890zertAZERTYUIOP^$\niQSDFGHJKLM%aaaWXCVBN,;:!cnv ";

  for (int i = 200; i < 256; i++) // Temp filling
    keymap[i] = 97 + (i % 26);

  for (int i = 0; map_azerty[i / 2] != '\0'; i += 2)
    keymap[i] = map_azerty[i / 2];

  init_key_buffer();
}

void init_key_buffer()
{
  u16 push_index = 0;
  u16 pop_index = 0;

  for (int i = 0; i < 256; i++)
    key_buffer[i] = BUFFER_DEFAULT;
}

void push_key(u8 keycode)
{
  if (push_index == pop_index && key_buffer[push_index] != BUFFER_DEFAULT)
    pop_index = (pop_index + 1) % 256;
  key_buffer[push_index] = keycode;
  push_index = (push_index + 1) % 256;
}

u8 pop_key()
{
  u8 code;

  code = key_buffer[pop_index];

  if (code != BUFFER_DEFAULT)
    {
      key_buffer[pop_index] = BUFFER_DEFAULT;
      pop_index = (pop_index + 1) % 256;
    }

  return (code);
}
